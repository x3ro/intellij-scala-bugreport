import shapeless._

case class Foo[L <: HList](list:L) {
  def length = list.runtimeLength
  def add[T](x:T): Foo[T :: L] = Foo(x :: list)
}

object Main extends App {
  println(Foo(5 :: HNil).length)
  println(Foo(HNil).add(42).length)
}
